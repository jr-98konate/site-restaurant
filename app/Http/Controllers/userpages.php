<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class userpages extends Controller
{
    // partie repas
    public function repas(){
        return view("repas");
    }

    // partie fastfood
    public function fasfood(){
        return view("fasfood");
    }
    // partie dessert
    public function desser(){
        return view("desser");
    }
    // partie boison
    public function boison(){
        return view("boison");
    }
    // partie fruit
    public function fruit(){
        return view("fruis");
    }
}
