@extends('base')

@section("header")

<section class="title-menu container-fluid mt-5">
    <div class="row justify-content-center">
        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
            <div class="name-menu">
                <h2>Arame  Resto</h2>
                <p>Jeudi 6 Oct 2022</p>
            </div>

        </div>
        <div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
            <div class="chose-menu">
                <h2>Choisir le plat</h2>
            </div>
        </div>
    </div>
    
</section>
<section class="container-fluid">
    <div class="row text-center">
        <div class="col col-lg-2">
            <div class="card" style="width: 8rem;">
                <img class="card-img-top" src="/assets/plat.png" alt="">
                <div class="card-block">
                    
                    <p>Spicy seasoned <br> seafood noodles</p>
                    <p>$ 2.29</p>
                    <div class="commande  text-center">
                        <a href="btn btn-md  mb-3">commande</a>
                    </div>
                    
                </div>
            </div>


        </div>
    </div>
</section>

@endsection