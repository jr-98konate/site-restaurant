<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.bunny.net/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">
        <!-- CSS only -->
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-rbsA2VBKQhggwzxH7pPCaAqO46MgnOM80zW1RWuH61DGLwZJEdK2Kadq2F9CUG65" crossorigin="anonymous">
        <link rel="stylesheet" href="./style.css">
        <link rel="stylesheet" href="path/to/font-awesome/css/font-awesome.min.css" 
    </head>
         <body >
            <nav class="navbar navbar-expand-lg bg-white">
                <div class="container-fluid">
                  <a class="navbar-brand" href="#"><img src="/assets/logo.png" alt=""></a>
                  <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                  </button>
                  <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav me-auto pe-auto mx-auto mb-2 mb-lg-0">
                      <li class="nav-item">
                        <a class="nav-link "  href="{{("/")}}">Repas</a>
                      </li>
                      <li class="nav-item">
                        <a class="nav-link" href="{{("/desser")}}">Dessert</a>
                      </li>
                      
                      <li class="nav-item">
                        <a class="nav-link" href="{{("/fruit")}}">Fruit</a>
                      </li>
                      <li class="nav-item">
                        <a class="nav-link" href="{{("/boison")}}">Boisons</a>
                      </li>
                      <li class="nav-item">
                        <a class="nav-link" href="{{("/food")}}">Fast food</a>
                      </li>
                    </ul>
                        <form class="d-flex me-5" role="search">
                            <div class="form-outline">
                                <input class="form-control me-2" type="search"  
                                placeholder="Search......" aria-label="Search">
                            </div>
                        </form>
                    
                    <div class="shoping">
                      <a href="" class="btn">
                        <img src="/assets/shoping.png" alt="">
                      </a>
                    </div>
                  </div>
                </div>
              </nav>
            @yield("header")
           

        
        
        
        <!-- JavaScript Bundle with Popper -->
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-kenU1KFdBIe4zVF0s0G1M5b4hcpxyD9F7jL+jjXkk+Q2h455rYXK/7HAuoJl+0I4" crossorigin="anonymous"></script>
    </body>
</html>
