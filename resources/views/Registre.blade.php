<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.bunny.net/css2?family=Nunito:wght@400;600;700&display=swap" rel="stylesheet">
        <!-- CSS only -->
        <link rel="stylesheet" href="./style.css">
        <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-rbsA2VBKQhggwzxH7pPCaAqO46MgnOM80zW1RWuH61DGLwZJEdK2Kadq2F9CUG65" crossorigin="anonymous">

        
    </head>
         <body >
            
            <section class="vh-100" style="background-color: #fff;">
                <div class="container py-5 h-100">
                  <div class="row d-flex justify-content-center align-items-center h-100">
                    <div class="col-12 col-md-8 col-lg-10 col-xl-6">
                      <div class="card shadow-2-strong " style="border-radius: 1rem;">
                        <div class="card-body p-5 text-center">
              
                          <h2 class="mb-2">Créez votre compte</h2>
                          <p class="mt-2 text-center"> Commençons par créer votre compte.Pour assurer la sécurité de votre compte,  nous avons besoin d'un mot de passe fort!</p>
              
                          <div class="form-outline mb-4">
                            <input type="email" id="typeEmailX-2" class="form-control form-control-lg" placeholder="Email"/>
                           
                          </div>
                          <div class="form-outline mb-4">
                            <input type="password" id="typeEmailX-2" class="form-control form-control-lg" placeholder="mot de passe"/>
                           
                          </div>
                          <div class="form-outline mb-4">
                            <input type="password" id="typeEmailX-2" class="form-control form-control-lg" placeholder="confirmer mot passe"/>
                           
                          </div>
              
                         
              
                         
              
                          <button class="btn btn btn-lg btn-block w-100" style="background-color: #EB966A; ">Continuer</button>
                         
              
                          
              
                          
                        
              
                        </div>
                      </div>
                    </div>
                  </div>
                </div>
              </section>
            
        
        
        <!-- JavaScript Bundle with Popper -->
        <script src="https://cdn.jsdelivr.net/npm/@popperjs/core@2.11.6/dist/umd/popper.min.js" integrity="sha384-oBqDVmMz9ATKxIep9tiCxS/Z9fNfEXiDAYTujMAeBAsjFuCZSmKbSSUnQlmh/jp3" crossorigin="anonymous"></script>
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/js/bootstrap.min.js" integrity="sha384-cuYeSxntonz0PPNlHhBs68uyIAVpIIOZZ5JqeqvYYIcEL727kskC66kF92t6Xl2V" crossorigin="anonymous"></script>
        <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.2.3/dist/js/bootstrap.bundle.min.js" integrity="sha384-kenU1KFdBIe4zVF0s0G1M5b4hcpxyD9F7jL+jjXkk+Q2h455rYXK/7HAuoJl+0I4" crossorigin="anonymous"></script>
    </body>
</html>
